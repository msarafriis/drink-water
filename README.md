# Drink Water

Tracks water that you've drank for the day.

This is part of a course by [Brad Traversy and Florin Pop](https://github.com/bradtraversy/50projects50days).
